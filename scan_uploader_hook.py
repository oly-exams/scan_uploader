#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Exam Tools
#
# Copyright (C) 2014 - 2018 Oly Exams Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import random
import datetime
import shutil
import re

logger = logging.getLogger('exam_tools.scan_uploader_hook')


def get_timestamp():
    return datetime.datetime.now().strftime('%Y%m%d_%H%M%S')


def create_unique_filename(filename):
    if re.match('2\d{7}_\d{6}_\d{3}_', filename) is None:
        return "{}_{:03d}_{}".format(get_timestamp(), random.randint(0,999), filename)
    else:
        return filename


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Pass scan document on to upload proxy')
    parser.add_argument('--received-dir', dest='received_dir', type=str, default='ftp-received', help='Directory for storing incoming documents')
    parser.add_argument('--dry-run', dest='dryrun', action='store_true', help='Skip writing to DB and filesystem')
    parser.add_argument('--move-file', dest='move_file', action='store_true', help='Move incoming files away')
    parser.add_argument('-vv', '--more-verbose', help="Be more verbose", action="store_const", dest="loglevel", const=logging.DEBUG, default=logging.WARNING)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const", dest="loglevel", const=logging.INFO)
    parser.add_argument('filename', help='Input PDF')
    args = parser.parse_args()

    ch = logging.StreamHandler()
    formatter = logging.Formatter('[%(asctime)s - %(name)s] - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.setLevel(args.loglevel)

    if not os.path.isdir(args.received_dir) and not args.dryrun:
        os.makedirs(args.received_dir)

    received_filename = os.path.join(args.received_dir, create_unique_filename(os.path.basename(args.filename)))
    if args.move_file:
        os.rename(args.filename, received_filename)
    else:
        shutil.copy(args.filename, received_filename)
