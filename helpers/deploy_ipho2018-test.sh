#!/bin/bash

ansible-playbook --vault-password-file=.vault-password-production.txt -i inventory/ipho2018-test deploy.yml $*
