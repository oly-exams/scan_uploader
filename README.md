# ExamTools Scan Uploader

## Deploy

Ansible scripts are provided for a local installation which configures the following services:
* **Pureftpd** for receiving the FTP documents
* **scan_uploader.py** for uploading the received documents to the main server in the cloud
* **scan_uploader_hook.py** which is triggered when a FTP document was received by pureftpd and stores in a format suitable for handling by scan_uploader.py

```bash
ansible-playbook --ask-pass -i inventory/XYZ deploy.yml
```
