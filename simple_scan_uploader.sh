RECEIVED=/opt/scan_uploader/ftp-received
UPLOADED=/opt/scan_uploader/ftp-uploaded
SLEEP=5

while true; do
	for file in $RECEIVED/*.pdf; do
		[[ -e $file ]] || continue; # if directory is empty, iteration is skipped.
		echo `date`: Uploading $file
		curl -S --ssl --insecure -T $file ftp://ipho-upload:5tPqVxh3ad@ibo2023.oly-exams.org
		mv $file $UPLOADED/
	done
	sleep $SLEEP
done

