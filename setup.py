#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from setuptools import setup, find_packages

import sys

readme = """Proxy scanned pdf documents to scan worker"""
version = '0.1'

setup(
    name='scan-uploader',
    py_modules=['scan_uploader'],
    version=version,
    url='http://oly-exams.org',
    author='Oly Exams Team',
    author_email='thomas.uehlinger@oly-exams.org',
    description=readme,
    install_requires=[],
    scripts=['scan_uploader.py', 'scan_uploader_hook.py'],
    license='GNU Affero General Public License',
)
