#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Exam Tools
#
# Copyright (C) 2014 - 2018 Oly Exams Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import os
import shutil
import glob
import subprocess
import logging
import sys


class MaxLevelFilter(logging.Filter):
    '''Filters (lets through) all messages with level < LEVEL'''
    def __init__(self, level):
        self.level = level

    def filter(self, record):
        return record.levelno < self.level # "<" instead of "<=": since logger.setLevel is inclusive, this should be exclusive


FTP_UPLOAD_SERVER = os.environ.get('FTP_UPLOAD_SERVER', 'server')
FTP_UPLOAD_USER = os.environ.get('FTP_UPLOAD_USER', 'user')
FTP_UPLOAD_PASS = os.environ.get('FTP_UPLOAD_PASS', 'password')
FTP_UPLOAD_INTERVAL = int(os.environ.get('FTP_UPLOAD_INTERVAL', 30))
FTP_RECEIVED_DIR = os.environ.get('FTP_RECEIVED_DIR', 'ftp-received')
FTP_UPLOADED_DIR = os.environ.get('FTP_UPLOADED_DIR', 'ftp-uploaded')

FILES_TO_KEEP = 100

logger = logging.getLogger('exam_tools.scan_uploader')

last_upload_time = time.time() - FTP_UPLOAD_INTERVAL


def upload_file(fn):
    logger.info('Trying to upload file {} to {}'.format(fn, FTP_UPLOAD_SERVER))

    ret = subprocess.call(["curl", "-sS", "--ssl", "--insecure", "-T", fn, "ftp://{}:{}@{}/{}".format(FTP_UPLOAD_USER, FTP_UPLOAD_PASS, FTP_UPLOAD_SERVER, os.path.basename(fn))])

    if ret == 0:
        logger.info('Successfully uploaded file.'.format(fn))
        return True
    else:
        logger.warning('Failed to upload file {} to {} (exit code {}), retrying in {} s'.format(fn, FTP_UPLOAD_SERVER, ret, FTP_UPLOAD_INTERVAL))
        return False


def delete_old_files(directory):
    # Get the list of files in the directory
    files = glob.glob(os.path.join(directory, '*'))
    files = [f for f in files if os.path.isfile(f)]

    # Sort the files based on modification time
    files.sort(key=os.path.getmtime)

    # Delete all files except the x most recent ones
    for file in files[:-FILES_TO_KEEP]:
        try:
            os.remove(file)
            logger.debug(f"Deleted file: {file}")
        except OSError:
            logger.warning(f"Unable to delete old file: {file}")
            

if __name__ == "__main__":
    MIN_LEVEL= logging.INFO

    stdout_hdlr = logging.StreamHandler(sys.stdout)
    stderr_hdlr = logging.StreamHandler(sys.stderr)
    lower_than_warning= MaxLevelFilter(logging.WARNING)
    stdout_hdlr.addFilter( lower_than_warning )     #messages lower than WARNING go to stdout
    stdout_hdlr.setLevel( MIN_LEVEL )
    stderr_hdlr.setLevel( max(MIN_LEVEL, logging.WARNING) ) #messages >= WARNING ( and >= STDOUT_LOG_LEVEL ) go to stderr

    formatter = logging.Formatter('[%(asctime)s - %(name)s] - %(levelname)s - %(message)s')
    stdout_hdlr.setFormatter(formatter)
    stderr_hdlr.setFormatter(formatter)
    logger.addHandler(stdout_hdlr)
    logger.addHandler(stderr_hdlr)
    logger.setLevel(logging.INFO)

    if not os.path.isdir(FTP_RECEIVED_DIR):
        os.makedirs(FTP_RECEIVED_DIR)
    if not os.path.isdir(FTP_UPLOADED_DIR):
        os.makedirs(FTP_UPLOADED_DIR)

    logger.info('Waiting for documents to be uploaded...')

    while True:
        files = sorted(glob.glob(os.path.join(FTP_RECEIVED_DIR, '*.pdf')))
        if len(files) > 0:
            fn = files[0]

            if upload_file(fn):
                delete_old_files(FTP_UPLOADED_DIR)
                os.rename(fn, os.path.join(FTP_UPLOADED_DIR, os.path.basename(fn)))

            delta = FTP_UPLOAD_INTERVAL - (time.time() - last_upload_time)
            last_upload_time = time.time()

            if delta > 0:
                time.sleep(delta)

        time.sleep(1)
